package fr.univlyon1.m2tiw.tiw1.server;

import fr.univlyon1.m2tiw.tiw1.metier.CinemaContext;
import fr.univlyon1.m2tiw.tiw1.metier.CinemaContextImpl;
import fr.univlyon1.m2tiw.tiw1.metier.CinemaResource;
import fr.univlyon1.m2tiw.tiw1.metier.CinemaResourceReservations;
import fr.univlyon1.m2tiw.tiw1.metier.CinemaResourceFilms;
import fr.univlyon1.m2tiw.tiw1.metier.CinemaResourceSalles;
import fr.univlyon1.m2tiw.tiw1.metier.CinemaResourceSeances;
import fr.univlyon1.m2tiw.tiw1.metier.dao.ReservationDao;
import fr.univlyon1.m2tiw.tiw1.metier.dao.ProgrammationDao;
import fr.univlyon1.m2tiw.tiw1.metier.dao.SallesDao;
import fr.univlyon1.m2tiw.tiw1.metier.dao.JsonProgrammationDao;
import fr.univlyon1.m2tiw.tiw1.metier.dao.JpaReservationDao;
import fr.univlyon1.m2tiw.tiw1.metier.dao.JsonSallesDao;
import fr.univlyon1.m2tiw.tiw1.utils.SeanceCompleteException;
import fr.univlyon1.m2tiw.tiw1.utils.SeanceNotFoundException;
import org.picocontainer.DefaultPicoContainer;
import org.picocontainer.MutablePicoContainer;
import org.picocontainer.behaviors.Caching;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.logging.Logger;

public class ServerImpl implements Server {

    private MutablePicoContainer cinemaContainer;

    private static final String sallesFile = "salles.json";
    private static final String filmsFile = "films.json";
    private static final String seancesFile = "seances.json";
    private static final String persistenceUnit = "pu";

    private static final Logger LOGGER = Logger.getLogger(ServerImpl.class.getName());

    /**
     * Deprecated ServerImpl constructor, it build a Cinema from various JSON files.
     * @throws IOException : fail to open a JSON file can throw a IOException.
     * @throws ParseException : fail to parse a JSON file can throw a ParseException.
     * @deprecated : use ServerImpl(CinemaContext context, String params) instead
     */
    public ServerImpl() throws IOException, ParseException {

        cinemaContainer = new DefaultPicoContainer(new Caching());

        // DAOs CONTEXT
        cinemaContainer.addComponent(CinemaContext.class, CinemaContextImpl.class);
        cinemaContainer.getComponent(CinemaContext.class).set(
                "salleDao", new JsonSallesDao(sallesFile));
        cinemaContainer.getComponent(CinemaContext.class).set(
                "programmationDao", new JsonProgrammationDao(
                (SallesDao) cinemaContainer.getComponent(CinemaContext.class).get("salleDao"),
                seancesFile,
                filmsFile
        ));
        cinemaContainer.getComponent(CinemaContext.class).set(
                "reservationDao", new JpaReservationDao(persistenceUnit));

        // RESOURCES
        cinemaContainer.addComponent(CinemaResourceFilms.class);
        cinemaContainer.addComponent(CinemaResourceSalles.class);
        cinemaContainer.addComponent(CinemaResourceSeances.class);
        cinemaContainer.addComponent(CinemaResourceReservations.class);

        cinemaContainer.start();
    }

    /**
     * Main constructor.
     * @param context : a context containing the dependencies
     * @param params : the paths to those dependencies
     */
    public ServerImpl(CinemaContext context, String params) {
        String[] param = params.split(",");

        cinemaContainer = new DefaultPicoContainer(new Caching());

        cinemaContainer.addComponent(context.get(param[0])); // CinemaResourceFilms
        cinemaContainer.addComponent(context.get(param[1])); // CinemaResourceSalles
        cinemaContainer.addComponent(context.get(param[2])); // CinemaResourceSeances
        cinemaContainer.addComponent(context.get(param[3])); // CinemaResourceReservation

        cinemaContainer.getComponent(CinemaResourceSeances.class)
                .setParentContainer(cinemaContainer);

        cinemaContainer.start();
    }

    /**
     * Main method of the Server.
     * @param method : action to perform (GET, ADD, DELETE, ...)
     * @param resource : target of the action (film, seance, ...)
     * @param parameters : extra parameters, (ADD, film, titre:TITRE, version:VO, ...)
     * @return : an Object, a String, null
     * @throws IOException : when a DAO fails.
     */
    public Object processRequest(
        String method,
        String resource,
        HashMap<String, String> parameters
    ) throws IOException, ParseException, SeanceNotFoundException, SeanceCompleteException {

        String res = "ERROR : ";
        CinemaResource cinemaResource;
        switch (resource) {
            case "film":
                cinemaResource = cinemaContainer.getComponent(
                        CinemaResourceFilms.class);
                break;
            case "salle":
                cinemaResource = cinemaContainer.getComponent(
                        CinemaResourceSalles.class);
                break;
            case "seance":
                cinemaResource = cinemaContainer.getComponent(
                        CinemaResourceSeances.class);
                break;
            case "reservation":
                cinemaResource = cinemaContainer.getComponent(
                        CinemaResourceReservations.class);
                break;
            case "all": // DEPRECATED
                LOGGER.warning("This Resource (all) is DEPRECATED and will be remove soon !");
                return res;
            default:
                res += "Unknown Resource [" + resource
                        + "], available resources are : film, salle, seance, reservation";
                return res;
        }
        return cinemaResource.process(method, parameters);
    }

    /**
     * Get all CinemaRessource objects.
     * @return : a Collection of CinemaResource
     * @deprecated : avoid using this method, retrieve objects via Context instead.
     */
    public Collection<CinemaResource> getResources() {
        Collection<CinemaResource> res = new ArrayList<>();

        res.add(cinemaContainer.getComponent(CinemaResourceFilms.class));
        res.add(cinemaContainer.getComponent(CinemaResourceSalles.class));
        res.add(cinemaContainer.getComponent(CinemaResourceSeances.class));
        res.add(cinemaContainer.getComponent(CinemaResourceReservations.class));

        return res;
    }

    /**
     * Get all the DAOs.
     * @return : a Collection of DAOs
     * @deprecated : avoid using this method, retrieve objects via Context instead.
     */
    public Collection<Object> getDaos() {
        Collection<Object> res = new ArrayList<>();
        CinemaContext context = cinemaContainer.getComponent(CinemaContext.class);
        res.add(context.get("salleDao"));
        res.add(context.get("programmationDao"));
        res.add(context.get("reservationDao"));
        return res;
    }

    /**
     * Get all the Metier objects.
     * @return : a Collection of objects.
     * @throws IOException : in case a DAOs fails.
     * @deprecated : avoid using this method, retrieve objects via Context instead.
     */
    public Collection<Object> getMetiers() throws IOException {
        Collection<Object> res = new ArrayList<>();
        Collection<Object> daos = getDaos();
        res.add(((SallesDao) daos.toArray()[0]).getListSalles());
        res.add(((ProgrammationDao) daos.toArray()[1]).getSeances());
        res.add(((ProgrammationDao) daos.toArray()[1]).getFilms());
        res.add(((ReservationDao) daos.toArray()[2]).getReservations());
        return res;
    }
}
