package fr.univlyon1.m2tiw.tiw1.utils;

public class SeanceNotFoundException extends Exception {
    public SeanceNotFoundException() {
        super("No Seance with this ID was found !");
    }
}
