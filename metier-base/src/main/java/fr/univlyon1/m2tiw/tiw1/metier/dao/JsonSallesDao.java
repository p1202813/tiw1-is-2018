package fr.univlyon1.m2tiw.tiw1.metier.dao;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import fr.univlyon1.m2tiw.tiw1.metier.CinemaContext;
import fr.univlyon1.m2tiw.tiw1.metier.Salle;
import fr.univlyon1.m2tiw.tiw1.metier.jsondto.SalleDto;
import fr.univlyon1.m2tiw.tiw1.metier.jsondto.SallesWrapper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class JsonSallesDao implements SallesDao {

    private static JavaType list_of_salles_type = TypeFactory.defaultInstance()
            .constructCollectionType(Collection.class, SalleDto.class);

    public ClassLoader classLoader = getClass().getClassLoader();

    private File sallesJson;

    private final ObjectMapper mapper = new ObjectMapper();

    private Map<String, Salle> salles;

    /**
     * Deprecated constructor, prefer the other constructor if possible.
     * @throws IOException : in case the dao fails
     * @deprecated : use JsonSallesDao(CinemaContext context, String params) instead
     */
    public JsonSallesDao() throws IOException {
        salles = new HashMap<>();
        sallesJson = new File(classLoader.getResource("sample-data/salles.json").getFile());
        Collection<SalleDto> sallesDto = mapper
                .readValue(sallesJson, SallesWrapper.class).salles;
        for (SalleDto dto : sallesDto) {
            salles.put(dto.nom, dto.asSalle());
        }
    }

    /**
     * Deprecated constructor.
     * @param file : a path to the JSON file
     * @throws IOException : in case the dao fails
     * @deprecated : use JsonSallesDao(CinemaContext context, String params) instead
     */
    public JsonSallesDao(String file) throws IOException {
        salles = new HashMap<>();
        sallesJson = new File(classLoader.getResource("sample-data/" + file).getFile());
        Collection<SalleDto> sallesDto = mapper
                .readValue(sallesJson, list_of_salles_type);
        for (SalleDto dto : sallesDto) {
            salles.put(dto.nom, dto.asSalle());
        }
    }

    /**
     * Main constructor.
     * @param context : a context containing the dependencies
     * @param params : the paths to those dependencies
     * @throws IOException : in case the DAO fails
     */
    public JsonSallesDao(CinemaContext context, String params) throws IOException {
        String[] param = params.split(",");
        String file = (String) context.get(param[0]);
        salles = new HashMap<>();
        sallesJson = new File(classLoader.getResource("sample-data/" + file).getFile());
        Collection<SalleDto> sallesDto = mapper
                .readValue(sallesJson, list_of_salles_type);
        for (SalleDto dto : sallesDto) {
            salles.put(dto.nom, dto.asSalle());
        }
    }

    @Override
    public Collection<Salle> getListSalles() throws IOException {
        Collection<Salle> res = new ArrayList<>();
        for (Salle s : salles.values()) {
            res.add(s);
        }
        return res;
    }

    private void save() throws IOException {
        Collection<SalleDto> sallesDtos = salles.values().stream()
                .map(SalleDto::fromSalle).collect(Collectors.toList());

        mapper.writeValue(sallesJson, sallesDtos);
    }

    @Override
    public void save(Salle salle) throws IOException {
        this.salles.put(salle.getNom(), salle);
        save();
    }

    @Override
    public void delete(Salle salle) throws IOException {
        this.salles.remove(salle);
        save();
    }
}
