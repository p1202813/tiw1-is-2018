package fr.univlyon1.m2tiw.tiw1.metier.dao;

import fr.univlyon1.m2tiw.tiw1.metier.Cinema;

import java.io.IOException;

public interface CinemaDao {
    public Cinema load() throws IOException;
}
