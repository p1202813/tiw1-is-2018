package fr.univlyon1.m2tiw.tiw1.metier;

import fr.univlyon1.m2tiw.tiw1.metier.dao.JpaReservationDao;
import fr.univlyon1.m2tiw.tiw1.metier.dao.JsonProgrammationDao;
import fr.univlyon1.m2tiw.tiw1.metier.dao.ProgrammationDao;
import fr.univlyon1.m2tiw.tiw1.metier.dao.ReservationDao;
import fr.univlyon1.m2tiw.tiw1.metier.dao.SallesDao;
import org.picocontainer.Startable;
import java.io.IOException;
import java.text.ParseException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Logger;

/**
 * @deprecated :  use an Annuaire/ServerImpl instead.
 */
public class Cinema implements Startable {
    private final String nom;
    private Map<String, Salle> salles;
    private Map<String, Film> films;
    private List<Seance> seances;
    private ReservationDao reservationDao;
    private ProgrammationDao programmationDao;
    private static final Logger LOGGER = Logger.getLogger(Cinema.class.getName());

    /**
     * Cinema main contructor.
     * @param nom : name of the Cinema.
     * @param salles : collection of the Cinema's Salle.
     * @throws IOException : fail to open a JSON file can throw a IOException.
     * @throws ParseException : fail to parse a JSON file can throw a ParseException.
     */
    public Cinema(String nom, Collection<Salle> salles) throws IOException, ParseException {
        this.nom = nom;
        this.salles = new HashMap<>();
        setSalles(salles);

        this.films = new HashMap<>();
        this.seances = new ArrayList<>();
        reservationDao = new JpaReservationDao();

        programmationDao = new JsonProgrammationDao(this.getSalles());
        setFilms(((JsonProgrammationDao) programmationDao).getFilms());
        setSeances(((JsonProgrammationDao) programmationDao).getListSeances());
        // Attention, les salles doivent être
        // cohérentes avec l'information contenue dans le fichier JSON des seances
    }

    /**
     * Cinema constructor with dependencies injection.
     * @param nom : name of the Cinema.
     * @param salles : collection of the Cinema's Sale.
     * @param jsonProgrammationDao : injected DAO.
     * @throws IOException : fail to open a JSON file can throw a IOException.
     * @throws ParseException : fail to parse a JSON file can throw a ParseException.
     */
    public Cinema(String nom,
                  Collection<Salle> salles,
                  JsonProgrammationDao jsonProgrammationDao,
                  JpaReservationDao jpaReservationDao) throws IOException, ParseException {
        this.nom = nom;
        this.salles = new HashMap<>();
        setSalles(salles);

        this.films = new HashMap<>();
        this.seances = new ArrayList<>();
        reservationDao = jpaReservationDao;

        programmationDao = jsonProgrammationDao;
        setFilms(((JsonProgrammationDao) programmationDao).getFilms());
        setSeances(((JsonProgrammationDao) programmationDao).getListSeances());
        // Attention, les salles doivent être
        // cohérentes avec l'information contenue dans le fichier JSON des seances
    }

    /**
     * Cinema constructor with dependencies injection.
     * @param nom : name of the Cinema.
     * @param sallesDao : collection of the Cinema's Sale.
     * @param programmationDao : injected DAO.
     * @throws IOException : fail to open a JSON file can throw a IOException.
     * @throws ParseException : fail to parse a JSON file can throw a ParseException.
     */
    public Cinema(String nom,
                  SallesDao sallesDao,
                  ProgrammationDao programmationDao,
                  ReservationDao reservationDao) throws IOException, ParseException {
        this.nom = nom;
        this.salles = new HashMap<>();
        setSalles(sallesDao.getListSalles());

        this.films = new HashMap<>();
        this.seances = new ArrayList<>();
        this.reservationDao = reservationDao;

        this.programmationDao = programmationDao;
        setFilms(((JsonProgrammationDao) this.programmationDao).getFilms());
        setSeances(((JsonProgrammationDao) this.programmationDao).getListSeances());
        // Attention, les salles doivent être
        // cohérentes avec l'information contenue dans le fichier JSON des seances
    }

    /**
     * Start method : do nothing except logging some info.
     */
    public void start() {
        LOGGER.info("Start Cinema component called '" + nom + "'");
        LOGGER.info("Programmation : " + programmationDao.toString());
        LOGGER.info("Reservation : " + reservationDao.toString());
    }

    /**
     * Stop method : do nothing except logging some info.
     */
    public void stop() {
        LOGGER.info("'" + nom + "' will be destroyed...");
    }

    /**
     * Main entry point of the Cinema class.
     * @param command : name of the action to perform
     * @param params : parameters
     * @return : an object or nothing depending on the command.
     */
    public Object process(String command, HashMap<String, String> params) {
        String res = "ERROR : ";
        switch (command) {
            case "get_nom":
                return getNom();
            case "get_nb_seances":
                return getNbSeances();
            case "get_salles":
                return getSalles();
            case "get_seances":
                return getSeances();
            case "get_films":
                return getFilms();
            case "get_salle":
                if (params.containsKey("name")) {
                    return getSalle(params.get("name"));
                }
                res += "no parameters 'name' in get_salle command => get_salle(name: SALLE_NAME)";
                break;
            case "get_film":
                if (params.containsKey("name")) {
                    return getFilm(params.get("name"));
                }
                res += "no parameters 'name' in get_film command => get_film(name: FILM_NAME)";
                break;
            default:
                res += "unknown command : [" + command + "] !"
                    + "\nAvailable commands are : "
                    + "get_nom, "
                    + "get_nb_seances, "
                    + "get_salles, "
                    + "get_seances, "
                    + "get_films, "
                    + "get_salle, "
                    + "get_film, ";
                break;
        }

        return res;
    }

    //
    // GETTERS ------------------------------------------------------
    //
    private String getNom() {
        return nom;
    }

    private int getNbSeances() {
        return seances.size();
    }

    private Collection<Salle> getSalles() {
        return salles.values();
    }

    private List<Seance> getSeances() {
        return seances;
    }

    private Collection<Film> getFilms() {
        return films.values();
    }

    private Salle getSalle(String salle) {
        return salles.get(salle);
    }

    private Film getFilm(String film) {
        return films.get(film);
    }

    //
    // ADDERS -------------------------------------------------------
    //
    private void addSalle(Salle salle) {
        this.salles.put(salle.getNom(), salle);
    }

    private void addFilm(Film film) throws IOException {
        this.films.put(film.getTitre() + " - " + film.getVersion(), film);
        programmationDao.save(film);
    }

    /**
     * Create a new Seance, add it to the Cinema.Seance List and persist it through DAO.
     * @param salle : Salle of the Seance.
     * @param film : Film of the Seance.
     * @param date : Date of the Seance.
     * @param prix : Price of the Seance.
     * @throws IOException : Persisting failure can throw an IOException.
     */
    private void createSeance(Salle salle, Film film, Date date, float prix) throws IOException {
        Seance seance = new Seance(film, salle, date, prix);
        this.seances.add(seance);
        programmationDao.save(seance);
        // seance.setReservationDao(reservationDao);
    }

    //
    // REMOVERS -----------------------------------------------------
    //
    private void removeSalle(Salle salle) {
        this.salles.remove(salle);
    }

    private void removeFilm(Film film) {
        this.films.remove(film);
    }

    private void removeSeance(Seance seance) throws IOException {
        seances.remove(seance);
        programmationDao.delete(seance);
    }

    //
    // SETTERS ------------------------------------------------------
    //
    /**
     * Set the entire Cinema.Salle Map by OVERWRITING the previous Map.
     * @param nSalles : Collection of new Salles.
     */
    private void setSalles(Collection<Salle> nSalles) {
        this.salles.clear();
        for (Salle s : nSalles) {
            addSalle(s);
        }
    }

    /**
     * Set the entire Cinema.Film Map by OVERWRITING the previous Map.
     * @param nFilms : Collection of new Films.
     * @throws IOException : this methods use addFilm, wich can throw IOException
     */
    private void setFilms(Collection<Film> nFilms) throws IOException {
        this.films.clear();
        for (Film f : nFilms) {
            addFilm(f);
        }
    }

    /**
     * Set the entire Cinema.Seance List by OVERWRITING the previous Lis.
     * @param seances : List of new Seances.
     */
    private void setSeances(List<Seance> seances) {
        this.seances = seances;
        // for (Seance s : seances) {
        //  s.setReservationDao(reservationDao);
        // }
    }

}
