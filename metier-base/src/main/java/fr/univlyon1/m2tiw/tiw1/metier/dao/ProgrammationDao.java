package fr.univlyon1.m2tiw.tiw1.metier.dao;

import fr.univlyon1.m2tiw.tiw1.metier.Cinema;
import fr.univlyon1.m2tiw.tiw1.metier.Film;
import fr.univlyon1.m2tiw.tiw1.metier.Salle;
import fr.univlyon1.m2tiw.tiw1.metier.Seance;
import fr.univlyon1.m2tiw.tiw1.utils.SeanceNotFoundException;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface ProgrammationDao {
    void initData(Cinema cinema) throws IOException;

    Seance getSeanceById(String id) throws SeanceNotFoundException;

    Salle getSalleByNom(String nom);

    Film getFilmByTitreVersion(String titre, String version);

    Collection<Seance> getSeanceByFilm(Film film);

    List<Film> getFilms();

    Map<String, Seance> getSeances();

    Collection<Seance> getSeancesCollection();

    void save(Seance seance) throws IOException;

    void save(Film film) throws IOException;

    void delete(Seance seance) throws IOException;

    void delete(Film film) throws IOException;
}
