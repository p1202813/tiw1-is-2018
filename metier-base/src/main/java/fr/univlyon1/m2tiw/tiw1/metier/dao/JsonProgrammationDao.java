package fr.univlyon1.m2tiw.tiw1.metier.dao;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.type.TypeFactory;
import fr.univlyon1.m2tiw.tiw1.metier.CinemaContext;
import fr.univlyon1.m2tiw.tiw1.metier.Film;
import fr.univlyon1.m2tiw.tiw1.metier.Seance;
import fr.univlyon1.m2tiw.tiw1.metier.Salle;
import fr.univlyon1.m2tiw.tiw1.metier.Cinema;
import fr.univlyon1.m2tiw.tiw1.metier.jsondto.FilmDto;
import fr.univlyon1.m2tiw.tiw1.metier.jsondto.SeanceDto;
import fr.univlyon1.m2tiw.tiw1.utils.SeanceNotFoundException;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.stream.Collectors;

import static fr.univlyon1.m2tiw.tiw1.metier.jsondto.CinemaDto.DATE_PARSER;

public class JsonProgrammationDao implements ProgrammationDao {

    public ClassLoader classLoader = getClass().getClassLoader();

    public File seanceJson;
    public File filmJson;

    private static ObjectMapper mapper = new ObjectMapper();

    static {
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
    }

    private static JavaType list_of_seances_type = TypeFactory.defaultInstance()
            .constructCollectionType(Collection.class, SeanceDto.class);
    private static JavaType list_of_films_type = TypeFactory.defaultInstance()
            .constructCollectionType(Collection.class, FilmDto.class);

    private List<Film> films = null;
    private Map<String, Seance> seances = null;
    private Map<String, Salle> salles;

    /**
     * Deprecated constructor, look for default path for seances and films JSON files.
     * @param salles : Collection of Salles.
     * @throws IOException : fail to open a JSON file can throw a IOException.
     * @throws ParseException : fail to parse a JSON file can throw a ParseException.
     * @deprecated : use JsonProgrammationDao(CinemaContext context, String params) instead
     */
    public JsonProgrammationDao(Collection<Salle> salles) throws IOException, ParseException {
        seanceJson = new File(classLoader.getResource("sample-data/seances.json").getFile());
        filmJson = new File(classLoader.getResource("sample-data/films.json").getFile());
        setSalles(salles);
        load();
    }

    /**
     * Deprecated constructor specifying a path inside
     * resources/sample-data/ for seance and film JSON files.
     * @param salles : Collection of Salles.
     * @param seancesFile : path to seances JSON file
     * @param filmsFile : path to films JSON file
     * @throws IOException : fail to open a JSON file can throw a IOException.
     * @throws ParseException : fail to parse a JSON file can throw a ParseException.
     * @deprecated : use JsonProgrammationDao(CinemaContext context, String params) instead
     */
    public JsonProgrammationDao(Collection<Salle> salles,
                                String seancesFile,
                                String filmsFile) throws IOException, ParseException {
        seanceJson = new File(classLoader.getResource("sample-data/" + seancesFile).getFile());
        filmJson = new File(classLoader.getResource("sample-data/" + filmsFile).getFile());
        setSalles(salles);
        load();
    }

    /**
     * Deprecated constructor specifying a path inside
     * resources/sample-data/ for seance and film JSON files.
     * @param dao : SallesDAO.
     * @param seancesFile : path to seances JSON file
     * @param filmsFile : path to films JSON file
     * @throws IOException : fail to open a JSON file can throw a IOException.
     * @throws ParseException : fail to parse a JSON file can throw a ParseException.
     * @deprecated : use JsonProgrammationDao(CinemaContext context, String params) instead
     */
    public JsonProgrammationDao(SallesDao dao,
                                String seancesFile,
                                String filmsFile) throws IOException, ParseException {
        seanceJson = new File(classLoader.getResource("sample-data/" + seancesFile).getFile());
        filmJson = new File(classLoader.getResource("sample-data/" + filmsFile).getFile());
        setSalles(dao.getListSalles());
        load();
    }

    /**
     * Main constructor.
     * @param context : a context containing the dependencies
     * @param params : the paths to those dependencies
     * @throws IOException : in case the DAO fails
     * @throws ParseException :  in case the DAO fails
     */
    public JsonProgrammationDao(CinemaContext context, String params)
            throws IOException, ParseException {
        String[] param = params.split(",");
        SallesDao dao = (SallesDao) context.get(param[0]);
        String seancesFile = (String) context.get(param[1]);
        String filmsFile = (String) context.get(param[2]);
        seanceJson = new File(classLoader.getResource("sample-data/" + seancesFile).getFile());
        filmJson = new File(classLoader.getResource("sample-data/" + filmsFile).getFile());
        setSalles(dao.getListSalles());
        load();
    }

    public List<Film> getFilms() {
        return films;
    }

    public Map<String, Seance> getSeances() {
        return seances;
    }

    /**
     * Get all the Seances.
     * @return : a Collection of Seances
     */
    public Collection<Seance> getSeancesCollection() {
        Collection<Seance> res = new ArrayList<>();

        for (Seance s : seances.values()) {
            res.add(s);
        }

        return res;
    }

    /**
     * Convert the Seances HashMap into a List.
     * @return : the converted List of Seances.
     */
    public List<Seance> getListSeances() {
        List<Seance> res = new ArrayList<>();
        for (Seance seance : seances.values()) {
            res.add(seance);
        }
        return res;
    }

    /**
     * Create a new Programmation Salle Collection by OVERWRITING the previous one.
     * @param salles : new Collection of Salles.
     */
    public void setSalles(Collection<Salle> salles) {
        this.salles = new HashMap<>();
        for (Salle s : salles) {
            this.salles.put(s.getNom(), s);
        }
    }

    private void load() throws IOException, ParseException {
        films = new ArrayList<>();
        seances = new HashMap<>();
        if (filmJson.exists()) {
            Collection<FilmDto> filmDtos = mapper.readValue(filmJson, list_of_films_type);
            films.addAll(filmDtos.stream().map(FilmDto::asFilm).collect(Collectors.toList()));
            if (seanceJson.exists()) {
                Collection<SeanceDto> seanceDtos =
                        mapper.readValue(seanceJson, list_of_seances_type);

                for (SeanceDto dto : seanceDtos) {
                    Seance s = new Seance(
                        getFilmById(dto.film),
                        salles.get(dto.salle),
                        DATE_PARSER.parse(dto.date),
                        dto.prix
                    );
                    seances.put(s.getId(), s);
                }
            }
        }
    }

    /**
     * Deprecated initialization method.
     * @param cinema : a cinema
     * @throws IOException : in case dao fails
     * @deprecated :  use an Annuaire/ServerImpl instead.
     */
    @Override
    public void initData(Cinema cinema) throws IOException {
        films = new ArrayList<>();
        films.addAll((Collection<Film>) cinema.process("get_films", new HashMap<>()));
        seances = new HashMap<>();
        for (Seance s : (Collection<Seance>) cinema.process("get_seances", new HashMap<>())) {
            seances.put(s.getId(), s);
        }
        save();
    }

    @Override
    public Seance getSeanceById(String id) throws SeanceNotFoundException {
        if (!seances.containsKey(id)) {
            throw new SeanceNotFoundException();
        }
        return seances.get(id);
    }

    @Override
    public Salle getSalleByNom(String nom) {
        return this.salles.get(nom);
    }

    @Override
    public Film getFilmByTitreVersion(String titre, String version) {
        for (Film f : films) {
            if (f.getTitre().equals(titre) && f.getVersion().equals(version)) {
                return f;
            }
        }
        return null;
    }

    private Film getFilmById(String id) {
        for (Film f : films) {
            if ((f.getTitre() + " - " + f.getVersion()).equals(id)) {
                return f;
            }
        }
        return null;
    }

    @Override
    public Collection<Seance> getSeanceByFilm(Film film) {
        Collection<Seance> result = new ArrayList<>();
        for (Seance s : seances.values()) {
            if (s.getFilm().equals(film)) {
                result.add(s);
            }
        }
        return result;
    }

    private void save() throws IOException {
        Collection<SeanceDto> seanceDtos = seances.values().stream()
                .map(SeanceDto::fromSeance).collect(Collectors.toList());

        mapper.writeValue(seanceJson, seanceDtos);
        Collection<FilmDto> filmDtos = films.stream().map(FilmDto::fromFilm)
                .collect(Collectors.toList());

        mapper.writeValue(filmJson, filmDtos);
    }

    @Override
    public void save(Seance seance) throws IOException {
        seances.put(seance.getId(), seance);
        save();
    }

    @Override
    public void save(Film film) throws IOException {
        int idx = films.indexOf(film);
        if (idx == -1) {
            films.add(film);
        } else {
            films.set(idx, film);
        }
        save();
    }

    @Override
    public void delete(Seance seance) throws IOException {
        seances.remove(seance);
        save();
    }

    @Override
    public void delete(Film film) throws IOException {
        films.remove(film);
        save();
    }
}
