package fr.univlyon1.m2tiw.tiw1.metier;

import fr.univlyon1.m2tiw.tiw1.metier.dao.SallesDao;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.logging.Logger;

public class CinemaResourceSalles extends CinemaResource {

    private Collection<Salle> salles;
    private SallesDao sallesDao;
    private static final Logger LOGGER = Logger.getLogger(CinemaResourceFilms.class.getName());

    /**
     * Deprecated constructor.
     * @param context : a context
     * @throws IOException : in case of the DAO fails.
     * @deprecated : use CinemaResourceSalles(CinemaContext context, String params) instead
     */
    public CinemaResourceSalles(CinemaContext context) throws IOException {
        this.salles = new ArrayList<>();
        this.sallesDao = (SallesDao) context.get("salleDao");
        this.salles = sallesDao.getListSalles();
    }

    /**
     * Main constructor.
     * @param context : a context containing the dependencies
     * @param params : the paths to those dependencies
     * @throws IOException : in case the DAO fails
     */
    public CinemaResourceSalles(CinemaContext context, String params) throws IOException {
        String[] param = params.split(",");
        this.salles = new ArrayList<>();
        this.sallesDao = (SallesDao) context.get(param[0]);
        this.salles = sallesDao.getListSalles();
    }

    /**
     * Start method : do nothing except logging some info.
     */
    public void start() {
        LOGGER.info("Start CinemaResource SALLES component !");
        LOGGER.info("Salles : " + sallesDao.toString());
    }

    /**
     * Stop method : do nothing except logging some info.
     */
    public void stop() {
        LOGGER.info("CinemaResource SALLES will be destroyed...");
    }

    /**
     * Main entry point of the class.
     * @param method : name of the action to perform
     * @param params : parameters
     * @return : an object or nothing depending on the command.
     */
    public Object process(String method, HashMap<String, String> params)
            throws IOException, NumberFormatException {
        String res = "ERROR : ";

        String nom = "";
        int capacite = -1;

        boolean hasNom = params.containsKey("nom");
        boolean hasCapacite = params.containsKey("capacite");

        if (hasNom) {
            nom = params.get("nom");
        }
        if (hasCapacite) {
            capacite = Integer.parseInt(params.get("capacite"));
        }

        switch (method) {
            case "GET":
                if (hasNom) {
                    return getSalle(nom);
                } else {
                    res += "GET Salle needs a 'nom' parameter !";
                }
                break;
            case "ADD":
                if (hasNom && hasCapacite) {
                    addSalle(new Salle(nom, capacite));
                    res = "SUCCESS : ADD salle !";
                } else {
                    res += "ADD Salle needs 'nom', and 'capacite' parameters !";
                }
                break;
            case "DELETE":
                if (hasNom && hasCapacite) {
                    removeSalle(new Salle(nom, capacite));
                    res = "SUCCESS : DELETE salle !";
                } else {
                    res += "DELETE Salle needs 'nom', and 'capacite' parameters !";
                }
                break;
            default:
                res += "UNKNOWN method [" + method + "], available methods are : GET, ADD, DELETE.";
                break;
        }

        return res;
    }

    private Salle getSalle(String nom) {
        for (Salle s : this.salles) {
            if (s.getNom().equals(nom)) {
                return s;
            }
        }
        return null;
    }

    private void addSalle(Salle salle) throws IOException {
        this.salles.add(salle);
        sallesDao.save(salle);
    }

    private void removeSalle(Salle salle) throws IOException {
        this.salles.removeIf(s -> s.getNom().equals(salle.getNom()));
        sallesDao.delete(salle);
    }
}
