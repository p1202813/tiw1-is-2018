package fr.univlyon1.m2tiw.tiw1.metier.jsondto;

import fr.univlyon1.m2tiw.tiw1.metier.Cinema;
import fr.univlyon1.m2tiw.tiw1.metier.Salle;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.stream.Collectors;

public class CinemaDto {
    public static final SimpleDateFormat DATE_PARSER =
            new SimpleDateFormat("yyyy-MM-dd HH:mm:ss zzz");

    public String nom;
    public Collection<SalleDto> salles;
    // public Collection<FilmDto> films;
    // public Collection<SeanceDto> seances;

    /**
     * Parse a JSON file into a Cinema Object.
     * @return : resulting Cinema object.
     * @throws ParseException : if the JSON Cinema is ill formed.
     * @throws IOException : if the file fail to open.
     */
    public Cinema asCinema() throws ParseException, IOException {
        Collection<Salle> sallesCinema = salles.stream()
                .map(SalleDto::asSalle).collect(Collectors.toList());

        // Cinema cinema = new Cinema(nom, sallesCinema);
        return new Cinema(nom, sallesCinema);
        // for (FilmDto f : films) {
        //  cinema.addFilm(f.asFilm());
        // }
        // for (SeanceDto s : seances) {
        //  Date d = DATE_PARSER.parse(s.date);
        //  cinema.createSeance(cinema.getSalle(s.salle), cinema.getFilm(s.film), d, s.prix);
        // }
        // return cinema;
    }

    public void setSalles(Collection<SalleDto> salles) {
        this.salles = salles;
    }
}
