package fr.univlyon1.m2tiw.tiw1.metier;

import fr.univlyon1.m2tiw.tiw1.metier.dao.ProgrammationDao;
import fr.univlyon1.m2tiw.tiw1.metier.dao.ReservationDao;
import fr.univlyon1.m2tiw.tiw1.utils.SeanceCompleteException;
import fr.univlyon1.m2tiw.tiw1.utils.SeanceNotFoundException;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

public class CinemaResourceReservations extends CinemaResource {
    private ProgrammationDao programmationDao;
    private ReservationDao reservationDao;
    private Map<Long, Reservation> reservations;
    private static final Logger LOGGER = Logger.getLogger(CinemaResourceFilms.class.getName());

    /**
     * Deprecated constructor.
     * @param context : a context
     * @deprecated : use CinemaResourceReservations(CinemaContext context, String params) instead
     */
    public CinemaResourceReservations(CinemaContext context) {
        this.reservations = new HashMap<>();
        this.reservationDao = (ReservationDao) context.get("reservationDao");
        this.programmationDao = (ProgrammationDao) context.get("programmationDao");
        setReservations();
    }

    /**
     * Main constructor.
     * @param context : a context containing the dependencies
     * @param params : the paths to those dependencies
     */
    public CinemaResourceReservations(CinemaContext context, String params) {
        String[] param = params.split(",");
        this.reservations = new HashMap<>();
        this.reservationDao = (ReservationDao) context.get(param[0]);
        this.programmationDao = (ProgrammationDao) context.get(param[1]);
        setReservations();
    }

    /**
     * Start method : do nothing except logging some info.
     */
    public void start() {
        LOGGER.info("Start CinemaResource RESERVATION component !");
        LOGGER.info("Programmation : " + programmationDao.toString());
        LOGGER.info("Reservation : " + reservationDao.toString());
    }

    /**
     * Stop method : do nothing except logging some info.
     */
    public void stop() {
        LOGGER.info("CinemaResource RESERVATION will be destroyed...");
    }

    /**
     * Class main entry point.
     * @param method : action to perform
     * @param params : optional parameters
     * @return : string or Reservation object
     * @throws IOException : in case the dao fails
     * @throws NumberFormatException : in case some params are not proper num
     * @throws SeanceCompleteException : in case a seance is full
     * @throws SeanceNotFoundException : in case a sid param is ill formed
     */
    public Object process(String method, HashMap<String, String> params)
        throws IOException, NumberFormatException,
        SeanceCompleteException, SeanceNotFoundException {

        String res = "ERROR : ";

        long rid = -1;
        String sid = "";
        String prenom = "";
        String nom = "";
        String email = "";

        boolean hasRid = params.containsKey("rid");
        if (hasRid) {
            rid = Long.parseLong(params.get("rid"));
        }
        boolean hasSid = params.containsKey("sid");
        if (hasSid) {
            sid = params.get("sid");
        }
        boolean hasPrenom = params.containsKey("prenom");
        if (hasPrenom) {
            prenom = params.get("prenom");
        }
        boolean hasNom = params.containsKey("nom");
        if (hasNom) {
            nom = params.get("nom");
        }
        boolean hasEmail = params.containsKey("email");
        if (hasEmail) {
            email = params.get("email");
        }

        switch (method) {
            case "GET":
                if (hasRid) {
                    return getReservation(rid);
                } else {
                    res += "GET Reservation needs a 'rid' parameter !";
                }
                break;
            case "ADD":
                if (hasSid && hasPrenom && hasNom && hasEmail) {
                    addReservation(sid, prenom, nom, email);
                    res = "SUCCESS : ADD Reservation !";
                } else {
                    res += "ADD Reservation needs 'sid', 'prenom', 'nom', and 'email' parameters !";
                }
                break;
            case "DELETE":
                if (hasRid) {
                    removeReservation(rid);
                    res = "SUCCESS : DELETE Reservation !";
                } else {
                    res += "DELETE Reservation needs a 'rid' parameter !";
                }
                break;
            default:
                res += "UNKNOWN method [" + method + "], available methods are : GET, ADD, DELETE.";
                break;
        }

        return res;
    }

    public Reservation getReservation(long id) {
        return this.reservations.get(id);
    }

    /**
     * Add a reservation.
     * @param sid : seance id
     * @param prenom : firstname of the customer
     * @param nom : lastname of the customer
     * @param email : email of the customer
     * @throws SeanceCompleteException : in case a seance is full
     * @throws SeanceNotFoundException : in case a sid param is ill formed
     */
    public void addReservation(String sid, String prenom, String nom, String email)
            throws SeanceNotFoundException, SeanceCompleteException {

        Seance s = programmationDao.getSeanceById(sid);
        if (s.getSalle().getCapacite() <= reservationDao.getBySeance(sid).size()) {
            throw new SeanceCompleteException();
        }

        Reservation r = new Reservation(prenom, nom, email);
        r.setSeanceId(sid);
        r.setPaye(true);
        reservationDao.save(r);
        this.reservations.put(r.getId(), r);
    }

    /**
     * Remove a reservation.
     * @param rid : reservation id
     */
    public void removeReservation(long rid) {
        Reservation r = reservationDao.getById(rid);
        this.reservations.remove(r.getId());
        reservationDao.delete(r);
    }

    private void setReservations() {
        this.reservations.clear();
        for (Reservation r : this.reservationDao.getReservations()) {
            this.reservations.put(r.getId(), r);
        }
    }
}
