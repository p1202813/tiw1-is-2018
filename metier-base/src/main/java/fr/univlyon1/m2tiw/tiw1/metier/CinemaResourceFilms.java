package fr.univlyon1.m2tiw.tiw1.metier;

import fr.univlyon1.m2tiw.tiw1.metier.dao.JsonProgrammationDao;
import fr.univlyon1.m2tiw.tiw1.metier.dao.ProgrammationDao;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

public class CinemaResourceFilms extends CinemaResource {

    private Map<String, Film> films;
    private ProgrammationDao programmationDao;
    private static final Logger LOGGER = Logger.getLogger(CinemaResourceFilms.class.getName());

    /**
     * Deprecated constructor.
     * @param context : a context
     * @throws IOException : in case the DAO fails
     * @deprecated : use CinemaResourceFilms(CinemaContext context, String params) instead
     */
    public CinemaResourceFilms(CinemaContext context) throws IOException {
        this.films = new HashMap<>();
        this.programmationDao = (ProgrammationDao) context.get("programmationDao");
        setFilms(((JsonProgrammationDao) this.programmationDao).getFilms());
    }

    /**
     * Main constructor.
     * @param context : a context containing the dependencies
     * @param params : the paths to those dependencies
     * @throws IOException : in case the DAO fails
     */
    public CinemaResourceFilms(CinemaContext context, String params) throws IOException {
        String[] param = params.split(",");
        this.films = new HashMap<>();
        this.programmationDao = (ProgrammationDao) context.get(param[0]);
        setFilms(((JsonProgrammationDao) this.programmationDao).getFilms());
    }

    /**
     * Start method : do nothing except logging some info.
     */
    public void start() {
        LOGGER.info("Start CinemaResource FILM component !");
        LOGGER.info("Programmation : " + programmationDao.toString());
    }

    /**
     * Stop method : do nothing except logging some info.
     */
    public void stop() {
        LOGGER.info("CinemaResource FILM will be destroyed...");
    }

    /**
     * Main entry point of the class.
     * @param method : name of the action to perform
     * @param params : parameters
     * @return : an object or nothing depending on the command.
     */
    public Object process(String method, HashMap<String, String> params) throws IOException {
        String res = "ERROR : ";

        String titre = "";
        String version = "";
        String fiche = "";

        boolean hasTitre = params.containsKey("titre");
        boolean hasVersion = params.containsKey("version");
        boolean hasFiche = params.containsKey("fiche");

        if (hasTitre) {
            titre = params.get("titre");
        }
        if (hasVersion) {
            version = params.get("version");
        }
        if (hasFiche) {
            fiche = params.get("fiche");
        }

        switch (method) {
            case "GET":
                if (hasTitre && hasVersion) {
                    return getFilm(titre, version);
                } else {
                    res += "GET Film needs a 'titre' parameter !";
                }
                break;
            case "ADD":
                if (hasTitre && hasVersion && hasFiche) {
                    addFilm(new Film(titre, version, fiche));
                    res = "SUCCESS : ADD film !";
                } else {
                    res += "ADD Film needs 'titre', 'version', and 'fiche' parameters !";
                }
                break;
            case "DELETE":
                if (hasTitre && hasVersion) {
                    removeFilm(titre, version);
                    res = "SUCCESS : DELETE film !";
                } else {
                    res += "DELETE Film needs 'titre', 'version', and 'fiche' parameters !";
                }
                break;
            default:
                res += "UNKNOWN method [" + method + "], available methods are : GET, ADD, DELETE.";
        }

        return res;
    }

    private Film getFilm(String titre, String version) {
        return films.get(titre + " - " + version);
    }

    private void addFilm(Film film) throws IOException {
        this.films.put(film.getTitre() + " - " + film.getVersion(), film);
        programmationDao.save(film);
    }

    private void removeFilm(String titre, String version) throws IOException {
        Film res = (Film) this.films.remove(titre + " - " + version);
        programmationDao.delete(res);
    }

    /**
     * Set the entire Cinema.Film Map by OVERWRITING the previous Map.
     * @param nFilms : Collection of new Films.
     * @throws IOException : this methods use addFilm, wich can throw IOException
     */
    private void setFilms(Collection<Film> nFilms) throws IOException {
        this.films.clear();
        for (Film f : nFilms) {
            addFilm(f);
        }
    }
}
