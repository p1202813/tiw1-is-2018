package fr.univlyon1.m2tiw.tiw1.metier;

import fr.univlyon1.m2tiw.tiw1.metier.dao.ProgrammationDao;
import org.picocontainer.MutablePicoContainer;
import org.picocontainer.behaviors.Caching;
import org.picocontainer.injectors.AnnotatedFieldInjection;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Logger;

public class CinemaResourceSeances extends CinemaResource {
    private SeancesListContainer seancesList;
    //private Map<String, Seance> seances;
    private ProgrammationDao programmationDao;
    private static final Logger LOGGER = Logger.getLogger(CinemaResourceFilms.class.getName());
    public static final SimpleDateFormat DATE_PARSER =
            new SimpleDateFormat("yyyy-MM-dd HH:mm:ss zzz");

    /**
     * Deprecated constructor.
     * @param context : a context
     * @deprecated : use CinemaResourceSeances(CinemaContext context, String params) instead
     */
    public CinemaResourceSeances(CinemaContext context) {
        //this.seances = new HashMap<>();
        this.programmationDao = (ProgrammationDao) context.get("programmationDao");

        //this.seances = programmationDao.getSeances();
    }

    /**
     * Main constructor.
     * @param context : a context containing the dependencies
     * @param params : the paths to those dependencies
     */
    public CinemaResourceSeances(CinemaContext context, String params) {
        String[] param = params.split(",");
        //this.seances = new HashMap<>();
        this.programmationDao = (ProgrammationDao) context.get(param[0]);

        //this.seances = programmationDao.getSeances();
    }

    /**
     * Instantiate the SeanceListContainer with its parent container.
     * @param parent : the parent container
     */
    public void setParentContainer(MutablePicoContainer parent) {
        seancesList = new SeancesListContainer(
                new Caching().wrap(new AnnotatedFieldInjection()), parent);
        seancesList.addAll(programmationDao.getSeancesCollection());
    }

    /**
     * Start method : do nothing except logging some info.
     */
    public void start() {
        LOGGER.info("Start CinemaResource SEANCES component !");
        LOGGER.info("Programmation : " + programmationDao.toString());
    }

    /**
     * Stop method : do nothing except logging some info.
     */
    public void stop() {
        LOGGER.info("CinemaResource SEANCES will be destroyed...");
    }

    /**
     * Main entry point of the class.
     * @param method : name of the action to perform
     * @param params : parameters
     * @return : an object or nothing depending on the command.
     */
    public Object process(String method, HashMap<String, String> params)
            throws IOException, ParseException {
        String res = "ERROR : ";

        String titre = "";
        String version = "";
        String nom = "";
        Date date = new Date();
        float prix = -1.0f;
        String id = "";

        boolean hasTitre = params.containsKey("titre");
        if (hasTitre) {
            titre = params.get("titre");
        }
        boolean hasVersion = params.containsKey("version");
        if (hasVersion) {
            version = params.get("version");
        }
        boolean hasNom = params.containsKey("nom");
        if (hasNom) {
            nom = params.get("nom");
        }
        boolean hasDate = params.containsKey("date");
        if (hasDate) {
            date = DATE_PARSER.parse(params.get("date"));
        }
        boolean hasPrix = params.containsKey("prix");
        if (hasPrix) {
            prix = Float.parseFloat(params.get("prix"));
        }
        boolean hasId = params.containsKey("id");
        if (hasId) {
            id = params.get("id");
        }

        switch (method) {
            case "GET":
                if (hasId) {
                    return getSeance(id);
                } else {
                    res += "GET Seance needs an 'id' parameter !";
                }
                break;
            case "ADD":
                if (hasTitre && hasVersion && hasNom && hasDate && hasPrix) {
                    addSeance(titre, version, nom, date, prix);
                    res = "SUCCESS : ADD Seance !";
                } else {
                    res += "ADD Seance needs "
                        + "'titre', 'version', 'nom', 'date', and 'prix' parameters !";
                }
                break;
            case "DELETE":
                if (hasId) {
                    removeSeance(id);
                    res = "SUCCESS : DELETE Seance !";
                } else {
                    res += "DELETE Seance needs an 'id' parameter !";
                }
                break;
            default:
                res += "UNKNOWN method [" + method + "], available methods are :"
                    + "GET, ADD, DELETE.";
        }

        return res;
    }

    /**
     * Get a Seance by id.
     * @param id : the id of the Seance
     * @return : the matching Seance
     */
    public Seance getSeance(String id) {
        //return this.seances.get(id);
        for (int i = 0; i < seancesList.size(); i++) {
            Seance s = (Seance) seancesList.get(i);
            if (s.getId().equals(id)) {
                return s;
            }
        }
        return null;
    }

    /**
     * Add a seance.
     * @param titre : the title of the film
     * @param version : the version of the film (VO, VF, ...)
     * @param nom : the name of the salle
     * @param date : the date
     * @param prix : the price
     * @throws IOException : in case the dao fails
     */
    public void addSeance(String titre, String version, String nom, Date date, float prix)
            throws IOException {
        Film film = programmationDao.getFilmByTitreVersion(titre, version);
        Salle salle = programmationDao.getSalleByNom(nom);
        Seance seance = new Seance(film, salle, date, prix);
        // this.seances.put(seance.getId(), seance);
        this.seancesList.add(seance);
        programmationDao.save(seance);
    }

    /**
     * Remove a seance.
     * @param id : the seance id
     * @throws IOException : in case the dao fails
     */
    public void removeSeance(String id) throws IOException {
        //Seance seance = this.seances.remove(id);
        Seance seance = getSeance(id);
        seancesList.remove(seance);
        programmationDao.delete(seance);
    }
}
