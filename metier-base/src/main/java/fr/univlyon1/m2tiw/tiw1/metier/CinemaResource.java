package fr.univlyon1.m2tiw.tiw1.metier;

import fr.univlyon1.m2tiw.tiw1.utils.SeanceCompleteException;
import fr.univlyon1.m2tiw.tiw1.utils.SeanceNotFoundException;
import org.picocontainer.Startable;

import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;

public abstract class CinemaResource implements Startable {

    public abstract Object process(String method, HashMap<String, String> params)
            throws IOException, NumberFormatException,
            SeanceCompleteException, SeanceNotFoundException, ParseException;
}
