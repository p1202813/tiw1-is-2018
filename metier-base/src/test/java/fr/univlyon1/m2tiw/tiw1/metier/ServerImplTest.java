package fr.univlyon1.m2tiw.tiw1.metier;

import fr.univlyon1.m2tiw.tiw1.server.Server;
import fr.univlyon1.m2tiw.tiw1.server.ServerImpl;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ServerImplTest {

    public static Server server;

    @BeforeClass
    public static void AinitServer() throws Exception {
        Annuaire annuaire = new Annuaire("config.json");
        server = (ServerImpl) annuaire.get("/server/serverImpl");
        //server = new ServerImpl();
    }

    // FILM *********************************************************
    @Test
    public void BtestGetFilm() throws Exception {
        Film res = new Film("Burning", "VO", "https://www.imdb.com/title/tt7282468/?ref_=shtt_ov_tt");
        HashMap<String, String> params = new HashMap<>();
        params.put("titre", "Burning");
        params.put("version", "VO");
        assertEquals(res, server.processRequest( "GET", "film", params));
    }

    @Test
    public void CtestAddfilm() throws Exception {
        Film res = new Film("Test", "KZ", "https://ultrarandom.xyz");
        HashMap<String, String> params = new HashMap<>();
        params.put("titre", "Test");
        params.put("version", "KZ");
        params.put("fiche", "https://ultrarandom.xyz");
        server.processRequest("ADD", "film", params);
        assertEquals(res, server.processRequest( "GET", "film", params));
    }

    @Test
    public void DtestRemovefilm() throws Exception {
        Film res = new Film("Test", "KZ", "https://ultrarandom.xyz");
        HashMap<String, String> params = new HashMap<>();
        params.put("titre", "Test");
        params.put("version", "KZ");
        params.put("fiche", "https://ultrarandom.xyz");
        server.processRequest("ADD", "film", params);
        assertEquals(res, server.processRequest( "GET", "film", params));

        server.processRequest("DELETE", "film", params);
        assertEquals(null, server.processRequest( "GET", "film", params));
    }

    // SALLES *******************************************************
    @Test
    public void EtestGetSalle() throws Exception {
        HashMap<String, String> params = new HashMap<>();
        params.put("nom", "Salle 1");
        Salle res = (Salle) server.processRequest("GET", "salle", params);
        assertEquals(100, res.getCapacite());
    }

    @Test
    public void FtestAddSalle() throws Exception {
        HashMap<String, String> params = new HashMap<>();
        params.put("nom", "test");
        params.put("capacite", "300");
        server.processRequest("ADD", "salle", params);
        Salle res = (Salle) server.processRequest("GET", "salle", params);
        assertEquals(300, res.getCapacite());
    }

    @Test
    public void GtestRemoveSalle() throws Exception {
        HashMap<String, String> params = new HashMap<>();
        params.put("nom", "test");
        params.put("capacite", "400");
        Salle res = (Salle) server.processRequest("GET", "salle", params);
        assertEquals(300, res.getCapacite());
        server.processRequest("DELETE", "salle", params);
        assertNull(server.processRequest("GET", "salle", params));
    }

    // SEANCES ******************************************************
    @Test
    public void HtestGetSeance() throws Exception {
        HashMap<String, String> params = new HashMap<>();
        params.put("id", "7926cce5-6539-30e0-9628-8ce5ed1d8211");
        Seance res = (Seance) server.processRequest("GET", "seance", params);
        assertEquals("Le poirier sauvage", res.getFilm().getTitre());
    }

    @Test
    public void ItestAddSeance() throws Exception {
        HashMap<String, String> params = new HashMap<>();
        params.put("titre", "Burning");
        params.put("version", "VO");
        params.put("nom", "Salle 1");
        params.put("date", "2018-11-03 15:00:00 CEST");
        params.put("prix", "7.50");
        server.processRequest("ADD", "seance", params);
        params.put("id", "116aac42-bef3-36d5-b6c8-3d6293e624af");
        Seance res = (Seance) server.processRequest("GET", "seance", params);
        assertEquals(100, res.getSalle().getCapacite());
    }

    @Test
    public void JtestRemoveSeance() throws Exception {
        HashMap<String, String> params = new HashMap<>();
        params.put("id", "116aac42-bef3-36d5-b6c8-3d6293e624af");
        server.processRequest("DELETE", "seance", params);
        assertNull(server.processRequest("GET", "seance", params));
    }

    // RESERVATIONS *************************************************
    @Test
    public void KtestGetReservation() throws Exception {
        // long id
        HashMap<String, String> params = new HashMap<>();
        params.put("rid", "1");
        Reservation res = (Reservation) server.processRequest("GET", "reservation", params);
        assertEquals("toto.machin@nowhere.net", res.getEmail());
    }

    @Test
    public void LtestAddReservation() throws Exception {
        HashMap<String, String> params = new HashMap<>();
        params.put("sid", "7926cce5-6539-30e0-9628-8ce5ed1d8211");
        params.put("prenom", "Vitalik");
        params.put("nom", "Buterin");
        params.put("email", "vitalik@ethereum.org");
        server.processRequest("ADD", "reservation", params);
        params.put("rid", "6");
        Reservation res = (Reservation) server.processRequest("GET", "reservation", params);
        assertEquals("vitalik@ethereum.org", res.getEmail());
    }

    @Test
    public void MtestRemoveReservation() throws Exception {
        HashMap<String, String> params = new HashMap<>();
        params.put("rid", "6");
        server.processRequest("DELETE", "reservation", params);
        assertNull(server.processRequest("GET", "reservation", params));
    }
}
