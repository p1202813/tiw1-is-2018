package fr.univlyon1.m2tiw.tiw1.metier.jsondto;

import fr.univlyon1.m2tiw.tiw1.metier.Film;
import fr.univlyon1.m2tiw.tiw1.metier.Salle;

public class SalleDto {
    public String nom;
    public int capacite;

    public Salle asSalle() {
        return new Salle(nom, capacite);
    }

    /**
     * Create a new SalleDto from a Salle object.
     * @param s : the Salle object
     * @return a new DTO.
     */
    public static SalleDto fromSalle(Salle s) {
        SalleDto dto = new SalleDto();
        dto.nom = s.getNom();
        dto.capacite = s.getCapacite();
        return dto;
    }
}
