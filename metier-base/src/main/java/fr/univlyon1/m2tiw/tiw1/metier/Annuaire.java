package fr.univlyon1.m2tiw.tiw1.metier;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class Annuaire {

    public ClassLoader classLoader = getClass().getClassLoader();

    private CinemaContext root;
    private static ObjectMapper mapper = new ObjectMapper();

    static {
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
    }

    /**
     * Main constructor.
     * @param configFile : path to the config file
     * @throws IOException : in case some daos fails
     */
    public Annuaire(String configFile) throws
            IOException, ClassNotFoundException, NoSuchMethodException,
            InstantiationException, IllegalAccessException, InvocationTargetException {
        this.root = new CinemaContextImpl();

        File configJson = new File(classLoader.getResource("sample-data/" + configFile).getFile());
        JsonNode configRoot = mapper.readTree(configJson);

        // STRINGS
        JsonNode stringsNode = configRoot.get("strings");
        for (JsonNode node : stringsNode) {
            this.root.set(node.get("path").asText(), node.get("value").asText());
        }

        // DAOs
        JsonNode daosNode = configRoot.get("daos");
        instantiateObjects(daosNode);

        // RESOURCES
        JsonNode resourcesNode = configRoot.get("resources");
        instantiateObjects(resourcesNode);

        // SERVER
        JsonNode serversNode = configRoot.get("servers");
        instantiateObjects(serversNode);
    }

    public Object get(String path) {
        return root.get(path);
    }

    public void set(String path, Object obj) {
        root.set(path, obj);
    }

    /**
     * Instantiate new Objects into the root context at a given path and with given parameters.
     * Those objects needs to have a constructor of (CinemaContext context, String params) !
     * @param arrayNode : A array of config json object
     * @throws ClassNotFoundException : in case Reflect API fails
     * @throws NoSuchMethodException : in case Reflect API fails
     * @throws InstantiationException : in case Reflect API fails
     * @throws IllegalAccessException : in case Reflect API fails
     * @throws InvocationTargetException : in case Reflect API fails
     */
    public void instantiateObjects(JsonNode arrayNode) throws
            ClassNotFoundException, NoSuchMethodException,
            InstantiationException, IllegalAccessException, InvocationTargetException {
        for (JsonNode node : arrayNode) {
            String className = node.get("class").asText();
            Class<?> objClass = Class.forName(className);
            Constructor<?> constructor =
                    objClass.getConstructor(CinemaContext.class, String.class);
            String params = node.get("params").asText();
            Object obj = constructor.newInstance(this.root, params);
            this.root.set(node.get("path").asText(), obj);
        }
    }
}
