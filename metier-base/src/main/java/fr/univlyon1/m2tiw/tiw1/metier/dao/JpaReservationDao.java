package fr.univlyon1.m2tiw.tiw1.metier.dao;

import fr.univlyon1.m2tiw.tiw1.metier.CinemaContext;
import fr.univlyon1.m2tiw.tiw1.metier.Reservation;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.util.Collection;

public class JpaReservationDao implements ReservationDao {

    private String persistenceUnitName;
    private EntityManager em;

    /**
     * Deprecated constructor.
     * @param em : a EntityManager
     * @deprecated : use JpaReservationDao(CinemaContext context, String params) instead
     */
    public JpaReservationDao(EntityManager em) {
        this.em = em;
    }

    /**
     * Deprecated constructor.
     * @deprecated : use JpaReservationDao(CinemaContext context, String params) instead
     */
    public JpaReservationDao() {
        persistenceUnitName = "pu";
        em = Persistence.createEntityManagerFactory(persistenceUnitName).createEntityManager();
    }

    /**
     * Deprecated constructor.
     * @param persistenceUnit : a String referencing the persistence unit
     * @deprecated : use JpaReservationDao(CinemaContext context, String params) instead
     */
    public JpaReservationDao(String persistenceUnit) {
        persistenceUnitName = persistenceUnit;
        em = Persistence.createEntityManagerFactory(persistenceUnitName).createEntityManager();
    }

    /**
     * Main constructor.
     * @param context : a context containing the dependencies
     * @param params : the paths to those dependencies
     */
    public JpaReservationDao(CinemaContext context, String params) {
        String[] param = params.split(",");
        persistenceUnitName = (String) context.get(param[0]);
        em = Persistence.createEntityManagerFactory(persistenceUnitName).createEntityManager();
    }

    @Override
    public void save(Reservation reservation) {
        // Ne devrait pas être ici, mais on verra ça avec les transactions déclaratives de Spring
        em.getTransaction().begin();
        if (reservation.getId() == null) {
            em.persist(reservation);
        } else {
            Reservation inEm = em.find(Reservation.class, reservation.getId());
            if (inEm != null) {
                em.merge(reservation);
            } else {
                em.persist(reservation);
            }
        }
        em.getTransaction().commit();
    }

    @Override
    public void delete(Reservation reservation) {
        Reservation persisted = getById(reservation.getId());
        if (persisted != null) {
            //Ne devrait pas être ici, mais on verra ça avec transactions déclaratives de Springmvn
            em.getTransaction().begin();
            em.remove(persisted);
            em.getTransaction().commit();
        }
    }

    @Override
    public Collection<Reservation> getBySeance(String seanceId) {
        return
                em.createNamedQuery("getBySeance", Reservation.class)
                        .setParameter(1, seanceId).getResultList();
    }

    @Override
    public Reservation getById(long id) {
        return em.find(Reservation.class, id);
    }

    @Override
    public Collection<Reservation> getReservations() {
        return
                em.createNamedQuery("getAll", Reservation.class).getResultList();
    }
}
