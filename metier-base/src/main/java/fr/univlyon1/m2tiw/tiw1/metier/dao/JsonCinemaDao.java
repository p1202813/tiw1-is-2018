package fr.univlyon1.m2tiw.tiw1.metier.dao;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.univlyon1.m2tiw.tiw1.metier.Cinema;
import fr.univlyon1.m2tiw.tiw1.metier.jsondto.CinemaDto;
import fr.univlyon1.m2tiw.tiw1.metier.jsondto.CinemaWrapper;
import fr.univlyon1.m2tiw.tiw1.metier.jsondto.SalleDto;
import fr.univlyon1.m2tiw.tiw1.metier.jsondto.SallesWrapper;

import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.util.Collection;

/**
 * @deprecated : use an Annuaire/ServerImpl instead.
 */
public class JsonCinemaDao implements CinemaDao {

    private static final URL RESOURCE_CINEMA = JsonCinemaDao.class
            .getResource("/sample-data/cinema.json");

    private static final URL RESOURCE_SALLES = JsonCinemaDao.class
            .getResource("/sample-data/salles.json");

    private final ObjectMapper mapper = new ObjectMapper();

    @Override
    public Cinema load() throws IOException {
        CinemaDto cinemaDto = mapper.readValue(RESOURCE_CINEMA, CinemaWrapper.class).cinema;
        Collection<SalleDto> sallesDto = mapper
                .readValue(RESOURCE_SALLES, SallesWrapper.class).salles;
        try {
            cinemaDto.setSalles(sallesDto);
            return cinemaDto.asCinema();
        } catch (ParseException e) {
            throw new IOException(e);
        }
    }
}
