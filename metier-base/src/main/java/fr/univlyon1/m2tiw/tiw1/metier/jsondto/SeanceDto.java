package fr.univlyon1.m2tiw.tiw1.metier.jsondto;

import fr.univlyon1.m2tiw.tiw1.metier.Seance;

public class SeanceDto {
    public String film;
    public String salle;
    public String date;
    public float prix;

    /**
     * Create a new SeanceDto from a Seance object.
     * @param seance : the Seance object.
     * @return the new DTO object.
     */
    public static SeanceDto fromSeance(Seance seance) {
        SeanceDto dto = new SeanceDto();
        dto.film = seance.getFilm().getTitre() + " - " + seance.getFilm().getVersion();
        dto.salle = seance.getSalle().getNom();
        dto.date = CinemaDto.DATE_PARSER.format(seance.getDate());
        dto.prix = seance.getPrix();
        return dto;
    }
}
