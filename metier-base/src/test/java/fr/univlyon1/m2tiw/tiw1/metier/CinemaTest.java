package fr.univlyon1.m2tiw.tiw1.metier;

import fr.univlyon1.m2tiw.tiw1.metier.dao.JpaReservationDao;
import fr.univlyon1.m2tiw.tiw1.metier.dao.JsonProgrammationDao;
import fr.univlyon1.m2tiw.tiw1.metier.dao.JsonSallesDao;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;

import static junit.framework.TestCase.assertEquals;

public class CinemaTest {

    private static final String salles_file = "salles.json";
    private static final String filmsFile = "films.json";
    private static final String seancesFile = "seances.json";
    private static JsonSallesDao jsonSallesDao;
    private static Collection<Salle> salles;
    private static JsonProgrammationDao jsonProgrammationDao;

    @BeforeClass
    public static void initDaos() throws Exception {
        jsonSallesDao = new JsonSallesDao(salles_file);
//        salles = jsonSallesDao.getListSalles();

        jsonProgrammationDao =
                new JsonProgrammationDao(jsonSallesDao, seancesFile, filmsFile);
    }

    @Test
    public void initCinema() throws Exception {
        Cinema cinema = new Cinema(
                "Mon ciné",
                jsonSallesDao,
                jsonProgrammationDao,
                new JpaReservationDao()
        );
    }

    @Test
    public void getNom() throws Exception {
        Cinema cinema = new Cinema(
                "Mon ciné",
                jsonSallesDao,
                jsonProgrammationDao,
                new JpaReservationDao()
        );
        assertEquals("Mon ciné", cinema.process("get_nom", new HashMap<>()));
    }

    @Test
    public void getNbSeances() throws Exception {
        Cinema cinema = new Cinema(
                "Mon ciné",
                jsonSallesDao,
                jsonProgrammationDao,
                new JpaReservationDao()
        );
        assertEquals(84, cinema.process("get_nb_seances", new HashMap<>()));
    }
}
