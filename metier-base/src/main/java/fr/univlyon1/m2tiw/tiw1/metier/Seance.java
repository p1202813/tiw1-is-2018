package fr.univlyon1.m2tiw.tiw1.metier;

import fr.univlyon1.m2tiw.tiw1.metier.dao.ReservationDao;
import fr.univlyon1.m2tiw.tiw1.utils.SeanceCompleteException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.Objects;

public class Seance {
    private final Film film;
    private final Salle salle;
    private final java.util.Date date;
    private final float prix;
    private List<Reservation> reservations;
    private final String id;
    private ReservationDao reservationDao;

    /**
     * Seance main constructor.
     * @param film : the Film object.
     * @param salle : the Salle.
     * @param date : a Date.
     * @param prix : the price.
     */
    public Seance(Film film, Salle salle, Date date, float prix) {
        this.film = film;
        this.salle = salle;
        this.date = date;
        this.prix = prix;
        this.reservations = new ArrayList<Reservation>();
        this.id = UUID.nameUUIDFromBytes(
                (film.getTitre()
                        + film.getVersion()
                        + salle.getNom()
                        + date.toString())
                        .getBytes())
                .toString();
    }

    public Film getFilm() {
        return film;
    }

    public Salle getSalle() {
        return salle;
    }

    public Date getDate() {
        return date;
    }

    public float getPrix() {
        return prix;
    }

    public void setReservationDao(ReservationDao reservationDao) {
        this.reservationDao = reservationDao;
    }

    /**
     * Create a new Reservation in Seance and persist it through DAO.
     * @param prenom : the first name of the customer.
     * @param nom : the last name of the customer.
     * @param email : the email address of the customer.
     * @return the newly created Reservation object.
     * @throws SeanceCompleteException : no Reservation can be added anymore, the Sceance is full.
     */
    public Reservation createReservation(String prenom, String nom, String email)
            throws SeanceCompleteException {

        if (this.salle.getCapacite() <= this.reservations.size()) {
            throw new SeanceCompleteException();
        }
        Reservation resa = new Reservation(prenom, nom, email);
        this.reservations.add(resa);
        resa.setSeanceId(getId());
        resa.setPaye(true);
        if (reservationDao != null) {
            reservationDao.save(resa);
        }
        return resa;
    }

    /**
     * Cancel a Reservation by selecting it and deleting it form the Seance AND the DAO.
     * @param reservation : the Reservation object to delete
     */
    public void cancelReservation(Reservation reservation) {
        this.reservations.remove(reservation);
        if (reservationDao != null) {
            reservationDao.delete(reservation);
        }
    }

    public String getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Seance seance = (Seance) o;
        return Objects.equals(id, seance.id);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }
}
