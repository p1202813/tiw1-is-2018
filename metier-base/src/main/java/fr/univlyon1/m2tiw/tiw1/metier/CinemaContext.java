package fr.univlyon1.m2tiw.tiw1.metier;


public interface CinemaContext {

    Object get(String path);

    void set(String path, Object obj);
}
