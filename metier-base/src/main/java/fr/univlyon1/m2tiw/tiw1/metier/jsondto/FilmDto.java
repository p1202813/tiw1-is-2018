package fr.univlyon1.m2tiw.tiw1.metier.jsondto;

import fr.univlyon1.m2tiw.tiw1.metier.Film;

public class FilmDto {
    public String titre;
    public String version;
    public String fiche;

    public Film asFilm() {
        return new Film(titre, version, fiche);
    }

    /**
     * Create a new FilmDto from a Film object.
     * @param f : the Film object
     * @return a new DTO.
     */
    public static FilmDto fromFilm(Film f) {
        FilmDto dto = new FilmDto();
        dto.titre = f.getTitre();
        dto.version = f.getVersion();
        dto.fiche = f.getFiche();
        return dto;
    }
}
