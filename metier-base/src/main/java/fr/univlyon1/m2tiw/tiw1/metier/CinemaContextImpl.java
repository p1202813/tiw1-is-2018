package fr.univlyon1.m2tiw.tiw1.metier;

import java.util.HashMap;

public class CinemaContextImpl implements CinemaContext {


    private HashMap<String, Object> ctx;

    public CinemaContextImpl() {
        this.ctx = new HashMap<>();
    }

    /**
     * Get an object from a Context giving a path.
     * @param path : the path of the object to retrieve
     * @return : the object at the end of the path
     */
    public Object get(String path) {
        String[] levels = path.split("/");
        if (levels.length > 1) {
            CinemaContext next = (CinemaContext) ctx.get(levels[0]);
            String newPath = createNewPath(levels);
            return next.get(newPath);
        } else if (levels.length == 1) {
            return ctx.get(levels[0]);
        }
        return this.ctx.get(path);
    }

    /**
     * Put a new object under some context path, if contexts doesn't exists, they will be created.
     * @param path : a context path
     * @param obj : the new object to insert
     */
    public void set(String path, Object obj) {
        String[] levels = path.split("/");
        if (levels.length > 1) {
            if (!ctx.containsKey(levels[0])) {
                ctx.put(levels[0], new CinemaContextImpl());
            }
            String newPath = createNewPath(levels);
            CinemaContext next = (CinemaContext) ctx.get(levels[0]);
            next.set(newPath, obj);
        } else if (levels.length == 1) {
            this.ctx.put(levels[0], obj);
        }
    }

    private static String createNewPath(String[] levels) {
        String newPath = "";
        for (int i = 1; i < levels.length; i++) {
            newPath += levels[i] + "/";
        }
        return newPath;
    }
}
