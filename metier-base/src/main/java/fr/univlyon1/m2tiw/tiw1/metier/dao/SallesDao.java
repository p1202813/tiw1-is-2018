package fr.univlyon1.m2tiw.tiw1.metier.dao;

import fr.univlyon1.m2tiw.tiw1.metier.Salle;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

public interface SallesDao {
    Collection<Salle> getListSalles() throws IOException;

    void save(Salle salle) throws IOException;

    void delete(Salle salle) throws IOException;
}
