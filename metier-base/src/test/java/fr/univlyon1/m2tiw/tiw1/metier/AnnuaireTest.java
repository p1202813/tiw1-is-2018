package fr.univlyon1.m2tiw.tiw1.metier;

import fr.univlyon1.m2tiw.tiw1.metier.dao.JsonProgrammationDao;
import fr.univlyon1.m2tiw.tiw1.metier.dao.ProgrammationDao;
import fr.univlyon1.m2tiw.tiw1.server.Server;
import fr.univlyon1.m2tiw.tiw1.server.ServerImpl;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static junit.framework.TestCase.assertNotNull;

public class AnnuaireTest {
    private static Annuaire annuaire;

    @BeforeClass
    public static void initAnnuaire() throws Exception {
        annuaire = new Annuaire("config.json");
    }

    @Test
    public void testGetServer() throws Exception {
        Server server = (ServerImpl) annuaire.get("/server/serverImpl");
        assertNotNull(server);
    }

    @Test
    public void testGetResource() throws Exception {
        CinemaResourceFilms films = (CinemaResourceFilms) annuaire.get("/server/resources/resourceFilms");
        assertNotNull(films);
    }

    @Test
    public void testGetDao() throws Exception {
        ProgrammationDao programmationDao = (JsonProgrammationDao) annuaire.get("/server/daos/programmationsDao");
        assertNotNull(programmationDao);
    }

    // @Test // DEPRECATED
    // public void testGetMetier() throws Exception {
    //     HashMap<String, Seance> seances = (HashMap<String, Seance>) annuaire.get("/server/metier/seances");
    //     assertNotNull(seances);
    // }
}
