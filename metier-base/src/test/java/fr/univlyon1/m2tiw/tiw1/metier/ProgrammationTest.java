package fr.univlyon1.m2tiw.tiw1.metier;

import fr.univlyon1.m2tiw.tiw1.metier.dao.JpaReservationDao;
import fr.univlyon1.m2tiw.tiw1.metier.dao.JsonCinemaDao;
import fr.univlyon1.m2tiw.tiw1.metier.dao.JsonProgrammationDao;
import fr.univlyon1.m2tiw.tiw1.metier.dao.JsonSallesDao;
import fr.univlyon1.m2tiw.tiw1.utils.SeanceNotFoundException;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ProgrammationTest {

    private static final String salles_file = "salles.json";
    private static final String filmsFile = "films.json";
    private static final String seancesFile = "seances.json";
    private static JsonSallesDao jsonSallesDao;
    private static Collection<Salle> salles;
    private static JsonProgrammationDao jsonProgrammationDao;
    private static Cinema cinema;

    @BeforeClass
    public static void initDaos() throws Exception {
        jsonSallesDao = new JsonSallesDao(salles_file);
        salles = jsonSallesDao.getListSalles();

        jsonProgrammationDao =
                new JsonProgrammationDao(salles, seancesFile, filmsFile);

        cinema = new Cinema(
                "Mon ciné",
                salles,
                jsonProgrammationDao,
                new JpaReservationDao()
        );
    }

    @Test
    public void testEcritFichiers() throws IOException {
        jsonProgrammationDao.initData(cinema);
    }

    @Test
    public void testGetSeancesByFilm() throws IOException {
        jsonProgrammationDao.initData(cinema);
        Film mi = jsonProgrammationDao.getFilmByTitreVersion("Mission Impossible - Fallout", "VF");
        Collection<Seance> seances = jsonProgrammationDao.getSeanceByFilm(mi);
        assertEquals(14, seances.size());
    }

    @Test
    public void testGetFilm() throws IOException {
        jsonProgrammationDao.initData(cinema);
        Film mi = jsonProgrammationDao.getFilmByTitreVersion("Mission Impossible - Fallout", "VF");
        assertNotNull(mi);
        assertEquals("https://www.imdb.com/title/tt4912910/?ref_=inth_ov_tt", mi.getFiche());
    }

    @Test
    public void testGetSeanceById() throws IOException, SeanceNotFoundException {
        jsonProgrammationDao.initData(cinema);
        ArrayList<Seance> seances = (ArrayList<Seance>) cinema.process("get_seances", new HashMap<>());
        Seance s = seances.get(0);
        Seance s2 = jsonProgrammationDao.getSeanceById(s.getId());
        assertEquals(s, s2);
    }
}
