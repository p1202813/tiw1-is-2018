package fr.univlyon1.m2tiw.tiw1.server;

import fr.univlyon1.m2tiw.tiw1.utils.SeanceCompleteException;
import fr.univlyon1.m2tiw.tiw1.utils.SeanceNotFoundException;

import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;

public interface Server {
    public Object processRequest(
        String method,
        String resource,
        HashMap<String, String> parametres
    ) throws IOException, ParseException, SeanceNotFoundException, SeanceCompleteException;
}
