package fr.univlyon1.m2tiw.tiw1.metier;

import org.picocontainer.ComponentFactory;
import org.picocontainer.DefaultPicoContainer;
import org.picocontainer.PicoContainer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.ListIterator;
import java.util.Iterator;


public class SeancesListContainer extends DefaultPicoContainer implements List {

    private ArrayList<Seance> seances;

    public SeancesListContainer(ComponentFactory factory, PicoContainer parent) {
        super(factory, parent);
        seances = new ArrayList<>();
    }

    @Override
    public Iterator iterator() {
        return seances.iterator();
    }

    @Override
    public ListIterator listIterator() {
        return seances.listIterator();
    }

    @Override
    public ListIterator listIterator(int i) {
        return seances.listIterator();
    }

    @Override
    public boolean remove(Object o) {
        return seances.remove(o);
    }

    @Override
    public Object remove(int i) {
        return seances.remove(i);
    }

    @Override
    public boolean removeAll(Collection collection) {
        return seances.removeAll(collection);
    }

    @Override
    public void add(int i, Object o) {
        seances.add(i, (Seance) o);
    }

    @Override
    public boolean add(Object o) {
        return seances.add((Seance) o);
    }

    @Override
    public boolean addAll(int i, Collection collection) {
        return seances.addAll(i,collection);
    }

    @Override
    public boolean addAll(Collection collection) {
        return seances.addAll(collection);
    }

    @Override
    public Object get(int i) {
        return seances.get(i);
    }

    @Override
    public Object set(int i, Object o) {
        return seances.set(i, (Seance) o);
    }

    @Override
    public List subList(int i, int i1) {
        return seances.subList(i, i1);
    }

    @Override
    public boolean isEmpty() {
        return seances.isEmpty();
    }

    @Override
    public int indexOf(Object o) {
        return seances.indexOf(o);
    }

    @Override
    public int lastIndexOf(Object o) {
        return seances.lastIndexOf(o);
    }

    @Override
    public int size() {
        return seances.size();
    }

    @Override
    public Object[] toArray() {
        return seances.toArray();
    }

    @Override
    public Object[] toArray(Object[] objects) {
        return seances.toArray(objects);
    }

    @Override
    public boolean contains(Object o) {
        return seances.contains(o);
    }

    @Override
    public boolean containsAll(Collection collection) {
        return seances.containsAll(collection);
    }

    @Override
    public void clear() {
        seances.clear();
    }

    @Override
    public boolean retainAll(Collection collection) {
        return seances.retainAll(collection);
    }
}
